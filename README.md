# README #

### Flubu example ###

* Example of flubu usage. Flubu is for building projects and executing deployment scripts using C# code.
* Version 1.0.0

### How do I get set up? ###

* Clone source code.
* Examine build script project especialy buildscript.cs https://bitbucket.org/mzorec/flubuexample/src/a285ffc7fda319277f2a49ea460b6d3559e2514f/BuildScript.cs?fileviewer=file-view-default

* run build.exe to run default build action. Build (example) will compile code, run unit tests, create iis web appilcation and package example application into zip for deployment.
https://bitbucket.org/mzorec/flubuexample/src/ffeb0039198a2eb1ea8c3bc046fc38dc4f4716b0/Build.exe?fileviewer=file-view-default

* run build.exe help to see other available build actions.