﻿using System;
using System.Collections.Generic;
using System.IO;
using Flubu;
using Flubu.Builds;
using Flubu.Builds.Tasks.NuGetTasks;
using Flubu.Builds.Tasks.TestingTasks;
using Flubu.Builds.VSSolutionBrowsing;
using Flubu.Packaging;
using Flubu.Targeting;
using Flubu.Tasks.Iis;
using Flubu.Tasks.Iis.Iis7;
using Flubu.Tasks.Processes;
//css_imp BuildScripts\\ExampleCustomTask.cs;
namespace BuildScripts
{
    public class BuildScript : DefaultBuildScript
    {
        protected override void ConfigureBuildProperties(TaskSession session)
        {
            session.Properties.Set(BuildProps.MSBuildToolsVersion, "4.0");
            session.Properties.Set(BuildProps.NUnitConsolePath, @"packages\NUnit.ConsoleRunner.3.2.1\tools\nunit3-console.exe");
            session.Properties.Set(BuildProps.ProductId, "FlubuExample");
            session.Properties.Set(BuildProps.ProductName, "FlubuExample");
            session.Properties.Set(BuildProps.SolutionFileName, "FlubuExample.sln");
            session.Properties.Set(BuildProps.VersionControlSystem, VersionControlSystem.Mercurial);
        }

        protected override void ConfigureTargets(TargetTree targetTree, ICollection<string> args)
        {
            ////compile is a flubu built in target.
            targetTree.AddTarget("rebuild")
                .SetDescription("Rebuilds the project, runs tests and packages the build products.")
                .SetAsDefault()
                .DependsOn("compile", "unit.tests", "iis.install", "package");

            ////load solution is a flubu built in target.
            targetTree.AddTarget("unit.tests")
               .SetDescription("Runs unit tests on the project")
               .Do(x => TargetRunTests(x)).DependsOn("load.solution");

            targetTree
                .AddTarget("iis.install")
                .SetDescription("Creates application pool, web site and web application for flubu example.")
                .Do(TargetiisInstall);

            targetTree.GetTarget("fetch.build.version")
                .Do(TargetFetchBuildVersion);

            ////Target doesnt work because nuspec is not defined. It is just an example how to publish nuget package.
            targetTree.AddTarget("nuget")
                .SetDescription("Produces NuGet packages for reusable components and publishes them to the NuGet server")
                .Do(c => TargetNuGet(c, "FlubuExample")).DependsOn("fetch.build.version");

            targetTree.AddTarget("package")
              .SetDescription("Packages all the build products into ZIP files for deployment")
              .Do(TargetPackage).DependsOn("load.solution", "compile");

            targetTree.AddTarget("svcutil.example")
                .SetDescription("Example how to run external program (svcutil) with flubu")
                .Do(TargetSvcUtil);

            targetTree.AddTarget("customTask.example")
             .SetDescription("Example for custom flubu task.")
             .Do(TargetCustomTask);
        }

        private static void TargetRunTests(ITaskContext context)
        {
            var task = NUnitTask.ForNunitV3("FlubuExample.Tests");
            task.Execute(context);
        }

        private static void TargetFetchBuildVersion(ITaskContext context)
        {
            Version version = BuildTargets.FetchBuildVersionFromFile(context);
            context.Properties.Set(BuildProps.BuildVersion, version);
            context.WriteInfo("The build version will be {0}", version);
        }

        private static void TargetSvcUtil(ITaskContext context)
        {
            var task = new RunProgramTask("lib\\svcutil\\svcutil.exe");
            task
                .AddArgument(@"/help")
                .Execute(context);
        }

        private static void TargetCustomTask(ITaskContext context)
        {
            var task = new ExampleCustomTask();
            task.Execute(context);
        }

        private static void TargetiisInstall(ITaskContext context)
        {
            const string applicationPoolName = "DefaultAppPool";
            
            ICreateAppPoolTask createAppPoolTask = new Iis7CreateAppPoolTask();
            createAppPoolTask.ApplicationPoolName = applicationPoolName;
            createAppPoolTask.Mode = CreateApplicationPoolMode.UpdateIfExists;
            createAppPoolTask.Execute(context);

            IControlAppPoolTask controlAppPoolTask = new Iis7ControlAppPoolTask();
            controlAppPoolTask.ApplicationPoolName = applicationPoolName;
            controlAppPoolTask.Action = ControlApplicationPoolAction.Stop;
            controlAppPoolTask.FailIfNotExist = false;
            controlAppPoolTask.Execute(context);

            var createWebSiteTask = new Iis7CreateWebsiteTask();
            createWebSiteTask
                .WebsiteName("Flubu")
                .BindingProtocol("http")
                .Port(1000)
                .PhysicalPath(Path.GetFullPath(".\\FlubuExample"))
                .ApplicationPoolName(applicationPoolName)
                .WebsiteMode(CreateWebApplicationMode.DoNothingIfExists)
                .Execute(context);

            ICreateWebApplicationTask createWebApplicationTask = new Iis7CreateWebApplicationTask();
            createWebApplicationTask.Mode = CreateWebApplicationMode.DoNothingIfExists;
            createWebApplicationTask.ApplicationPoolName = applicationPoolName;
            createWebApplicationTask.AllowAnonymous = true;
            createWebApplicationTask.AllowAuthNtlm = false;
            createWebApplicationTask.WebsiteName = "Flubu";
            createWebApplicationTask.LocalPath = Path.GetFullPath(".\\FlubuExample\\FlubuExample");
            createWebApplicationTask.ApplicationName = "FlubuExample3";
            createWebApplicationTask.Execute(context);
          
            controlAppPoolTask.Action = ControlApplicationPoolAction.Start;
            controlAppPoolTask.Execute(context);
        }

        private static void TargetNuGet(ITaskContext context, string nugetId)
        {
            PublishNuGetPackageTask publishTask = new PublishNuGetPackageTask(nugetId, @"FlubuExample\Flubu.nuspec");
            publishTask.ForApiKeyUseEnvironmentVariable();
            publishTask.Execute(context);
        }

        private static void TargetPackage(ITaskContext context)
        {
            IDirectoryFilesLister directoryFilesLister = new DirectoryFilesLister();
            ICopier copier = new Copier(context);
            IZipper zipper = new Zipper(context);

            StandardPackageDef packageDef = new StandardPackageDef();

            FilterCollection installBinFilters = new FilterCollection();

            installBinFilters.Add(new RegexFileFilter(@".*\.xml$"));
            installBinFilters.Add(new RegexFileFilter(@".svn"));

            DirectorySource binSource = new DirectorySource(
                context,
                directoryFilesLister,
                "web.bin",
                GetProjectOutputDir("FlubuExample", context));
            binSource.SetFilter(installBinFilters);
            packageDef.AddFilesSource(binSource);

            FilterCollection filters = new FilterCollection();
            filters.Add(new RegexFileFilter(@".svn"));

            DirectorySource content = new DirectorySource(
               context,
               directoryFilesLister,
               "web.content",
               new FullPath("FlubuExample\\Content"),
               true);
            content.SetFilter(filters);
            packageDef.AddFilesSource(content);

            DirectorySource images = new DirectorySource(
               context,
               directoryFilesLister,
               "web.images",
               new FullPath("FlubuExample\\Images"),
               true);
            images.SetFilter(filters);
            packageDef.AddFilesSource(images);
           

            DirectorySource themes = new DirectorySource(
               context,
               directoryFilesLister,
               "web.themes",
               new FullPath("FlubuExample\\Views"),
               true);
            themes.SetFilter(filters);
            packageDef.AddFilesSource(themes);

            DirectorySource scripts = new DirectorySource(
               context,
               directoryFilesLister,
               "web.scripts",
               new FullPath("FlubuExample\\Scripts"),
               true);
            scripts.SetFilter(filters);
            packageDef.AddFilesSource(scripts);

            DirectorySource webSource = new DirectorySource(
                context,
                directoryFilesLister,
                "web",
                new FullPath("FlubuExample"),
                false);
            webSource.SetFilter(
                new NegativeFilter(new RegexFileFilter(@"^.*\.(svc|asax|config|js|html|ico|bat)$")));
            packageDef.AddFilesSource(webSource);

            FullPath buildPackagesDir = GetBuildPackagesDir(context);

            CopyProcessor copyProcessor = new CopyProcessor(
                context,
                copier,
                buildPackagesDir);
            copyProcessor
                .AddTransformation("web", new LocalPath(@"FlubuExample"))
                .AddTransformation("web.bin", new LocalPath(@"FlubuExample\bin"))
                .AddTransformation("web.content", new LocalPath(@"FlubuExample\Content"))
                .AddTransformation("web.images", new LocalPath(@"FlubuExample\Images"))
                .AddTransformation("web.themes", new LocalPath(@"FlubuExample\Themes"))
                .AddTransformation("web.scripts", new LocalPath(@"FlubuExample\Scripts"));

            IPackageDef copiedPackageDef = copyProcessor.Process(packageDef);

            Version buildVersion = context.Properties.Get<Version>(BuildProps.BuildVersion);

            ZipProcessor zipProcessor = new ZipProcessor(
                context,
                zipper,
                buildPackagesDir.AddFileName("FlubuExample-{0}.zip", buildVersion),
                buildPackagesDir,
                null,
                "web",
                "web.bin",
                "web.content",
                "web.images",
                "web.scripts",
                "web.themes");

            zipProcessor.Process(copiedPackageDef);
        }

        public static FullPath GetBuildPackagesDir(ITaskContext context)
        {
            return new FullPath(context.Properties[BuildProps.BuildDir])
                .CombineWith("packages");
        }

        public static FullPath GetProjectOutputDir(string projectName, ITaskContext context)
        {
            VSSolution solution = context.Properties.Get<VSSolution>(BuildProps.Solution);
            VSProjectWithFileInfo project = (VSProjectWithFileInfo)solution.FindProjectByName(projectName);

            return new FullPath(projectName)
                .CombineWith(project.GetProjectOutputPath(context.Properties.Get<string>(BuildProps.BuildConfiguration)));
        }
    }
}
